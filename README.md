# Prometheus

> Prometheus collects metrics from targets by scraping metrics HTTP endpoints. Since Prometheus exposes data in the same manner about itself, it can also scrape and monitor its own health.

 https://prometheus.io/

Construido a través de la imagen de GNU/Linux Debian de Sindominio

## Configuración

Hay una plantilla para el  _docker-compose.yml_, hay que adaptarla a cada infraestructura

De la misma manera, adjunto los templates de los ficheros de configuración:

* _alert.rules.yml_ : Configuración de las emisiones de alertas a AlertManager (tiempo de espera, intentos, ...) 
* _prometheus.yml_ : Configuración de los grupos/usuarios/exporters que debe controlar Prometheus
* _alertmanager.yml_ : Configuración del servicio de AlertManager para el envió de las notificaciones

Más información de la configuración:

* https://prometheus.io/docs/prometheus/latest/configuration/configuration/
* https://prometheus.io/docs/alerting/latest/configuration/

