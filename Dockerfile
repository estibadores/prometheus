FROM registry.sindominio.net/debian

RUN apt-get update && \
    apt-get -qy --no-install-recommends install prometheus ca-certificates &&\
    apt-get clean

VOLUME ["/etc/prometheus","/var/lib/prometheus"]

## Change LOG LEVEL on production
CMD /usr/bin/prometheus --log.level=debug
